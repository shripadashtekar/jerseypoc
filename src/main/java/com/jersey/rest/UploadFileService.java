package com.jersey.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/file")
public class UploadFileService {

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {

		convertToZip(uploadedInputStream);

		String output = "File Uploaded";

		return Response.status(200).entity(output).build();

	}

	private void convertToZip(InputStream uploadedInputStream) {

		byte[] buffer = new byte[1024];
		try {
			FileOutputStream fos = new FileOutputStream("/Users/shripadashtekar/Desktop/snackdown/op/output.zip");
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry("output.txt");
    		zos.putNextEntry(ze);
    		
    		int len;
    		while ((len = uploadedInputStream.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		uploadedInputStream.close();
    		zos.closeEntry();
    		zos.close();

    		System.out.println("Done");
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}